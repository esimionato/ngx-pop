import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Http, Headers } from '@angular/http';

declare const ga: any;
declare const token: any;

@Injectable()
export class HttpService {

  public headers: Headers;
  public lastResponse: any;
  public popUser: any;
  public popGame: any;
  public baseEndpoint: any;
  public paths: any;

  constructor(public http: Http) {
    this.headers = new Headers({'Content-Type': 'application/json'});
    this.popUser = {};
    this.popGame = {};
    this.baseEndpoint = `http://192.168.0.15:8080/api/`;
    //this.baseEndpoint = `http://172.16.48.35:8080/api/`;
    this.paths = {
      virtualCurrency: `/manager/virtual-currencies`,
      customData: `/manager/data-definitions`,
      catalogItem: `/manager/catalog-items`,
      playerData: `/manager/player-data`,
      globalData: `/manager/global-data`,
      statisticDefinition: `/manager/statistic-definitions`,
      eventDefinition: `/manager/event-definitions`,
      levellingCurve: `/manager/xp-level-definitions`,
      achievementDefinition: `/manager/achievement-definitions`,
      tournamentDefinition: `/manager/tournaments`,
      player: `/manager/players`,
    };
  }

  refreshGame() {
    console.log('refreshGame..');
    this.getGame(1).map(res => {
      this.popGame = res.json();
      console.log(this.popGame);
      return this.popGame;
    }).toPromise();
  }


  getGame(id: any): any {
    return this.http.get(this.baseEndpoint+`games/${this.popUser.gameId}`, { headers: this.headers });
  }


  doGet(endPoint: any): any {
    let url = this.baseEndpoint + this.popUser.titleId + endPoint;
    return this.http.get(url, { headers: this.headers });
  }

  doPost(endPoint: any, data: any): any {
    let url = this.baseEndpoint + this.popUser.titleId + endPoint;
    return this.http.post(url, data, { headers: this.headers });
  }

  doPut(endPoint: any, data: any): any {
    let url = this.baseEndpoint + this.popUser.titleId + endPoint;
    return this.http.put(url, data, { headers: this.headers });
  }

  doDelete(endPoint: any): any {
    let url = this.baseEndpoint + this.popUser.titleId + endPoint;
    return this.http.delete(url, { headers: this.headers });
  }

}
