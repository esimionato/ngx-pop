import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class CustomDataService {

  data = [
    {
      id: 1,
      data_key: 'heroe',
      scope: 'public',
      permissions: 'write',
      data_version: '3',
      default_value: '{color: \'rojo\', escudo: \'verde\'}'
    },
    {
      id: 2,
      data_key: 'mochila',
      scope: 'public',
      data_version: '18',
      permissions: 'write',
      default_value: '{leyenda: \'dragon rojo\'}'
    },
  ];

  constructor(protected http: Http) {
  }

  getData() {
    return this.data;
  }

    /*
  getRemoteData(): Promise<any> {
    return this.http.get(http.paths.customData).map(res => {
      //this.lastRequest = +res.headers.get('x-total-count');
      return res.json();
    }).toPromise();
  }
     */
}
