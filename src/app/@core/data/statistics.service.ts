import { Injectable } from '@angular/core';

@Injectable()
export class StatisticsService {

  private data = [
    {
      title: 'Nov 10',
      hours: [
        { hour: '00:00', delta: '0.16', down: true,  logins: '791', installs: '92' },
        { hour: '01:00', delta: '0.64', down: true,  logins: '803', installs: '94' },
        { hour: '02:00', delta: '0.97', down: true,  logins: '816', installs: '97' },
        { hour: '03:00', delta: '1.39', down: false, logins: '815', installs: '97' },
        { hour: '04:00', delta: '1.32', down: true,  logins: '809', installs: '96' },
        { hour: '05:00', delta: '1.83', down: true,  logins: '806', installs: '95' },
        { hour: '06:00', delta: '0.73', down: true,  logins: '807', installs: '95' },
        { hour: '07:00', delta: '0.05', down: true,  logins: '808', installs: '96' },
        { hour: '08:00', delta: '2.61', down: true,  logins: '792', installs: '92' },
        { hour: '09:00', delta: '1.71', down: true,  logins: '786', installs: '89' },
        { hour: '10:00', delta: '2.17', down: false, logins: '818', installs: '98' },
        { hour: '11:00', delta: '0.37', down: false, logins: '789', installs: '91' },
      ],
    },
    {
      title: 'Nov 11',
      active: true,
      hours: [
        { hour: '00:00', delta: '0.97', down: true,  logins: '816', installs: '97' },
        { hour: '01:00', delta: '1.83', down: true,  logins: '806', installs: '95' },
        { hour: '02:00', delta: '0.64', down: true,  logins: '803', installs: '94' },
        { hour: '03:00', delta: '2.17', down: false, logins: '818', installs: '98' },
        { hour: '04:00', delta: '1.32', down: true,  logins: '809', installs: '96' },
        { hour: '05:00', delta: '0.05', down: true,  logins: '808', installs: '96' },
        { hour: '06:00', delta: '1.39', down: false, logins: '815', installs: '97' },
        { hour: '07:00', delta: '0.73', down: true,  logins: '807', installs: '95' },
        { hour: '08:00', delta: '2.61', down: true,  logins: '792', installs: '92' },
        { hour: '09:00', delta: '0.16', down: true,  logins: '791', installs: '92' },
        { hour: '10:00', delta: '1.71', down: true,  logins: '786', installs: '89' },
        { hour: '11:00', delta: '0.37', down: false, logins: '789', installs: '91' },
      ],
    },
    {
      title: 'Nov 12',
      hours: [
        { hour: '00:00', delta: '0.73', down: true,  logins: '807', installs: '95' },
        { hour: '01:00', delta: '2.17', down: false, logins: '818', installs: '98' },
        { hour: '02:00', delta: '2.61', down: true,  logins: '792', installs: '92' },
        { hour: '03:00', delta: '1.32', down: true,  logins: '809', installs: '96' },
        { hour: '04:00', delta: '1.83', down: true,  logins: '806', installs: '95' },
        { hour: '05:00', delta: '0.05', down: true,  logins: '808', installs: '96' },
        { hour: '06:00', delta: '0.97', down: true,  logins: '816', installs: '97' },
        { hour: '07:00', delta: '0.37', down: false, logins: '789', installs: '91' },
        { hour: '08:00', delta: '1.39', down: false, logins: '815', installs: '97' },
        { hour: '09:00', delta: '0.64', down: true,  logins: '803', installs: '94' },
        { hour: '10:00', delta: '1.71', down: true,  logins: '786', installs: '89' },
        { hour: '11:00', delta: '0.16', down: true,  logins: '791', installs: '92' },
      ],
    },
  ];

  constructor() {
  }

  // TODO: observables
  getData() {
    return this.data;
  }
}
