import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './users.service';
import { ElectricityService } from './electricity.service';
import { StatisticsService } from './statistics.service';
import { StateService } from './state.service';
import { SmartTableService } from './smart-table.service';
//import { PlayerService } from './player.service';
import { PlayersService } from './players.service';
import { CustomDataService } from './custom-data.service';
import { PlayerDataService } from './player-data.service';
//import { PlaylistService } from './playlist.service';

const SERVICES = [
  UserService,
  ElectricityService,
  StatisticsService,
  StateService,
  SmartTableService,
  PlayersService,
  CustomDataService,
  PlayerDataService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
