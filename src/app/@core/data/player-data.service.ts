import { Injectable } from '@angular/core';

@Injectable()
export class PlayerDataService {

  data = [
    { id: 1, data_key: 'heroe', data_version: '3', value: '{color: \'rojo\', escudo: \'verde\'}' },
    { id: 2, data_key: 'mochila', data_version: '18', value: '{leyenda: \'dragon rojo\'}' },
    { id: 3, data_key: 'propieda#01', data_version: '01', value: '{leyenda: \'dragon rojo\'}' },
    { id: 4, data_key: 'propieda#02', data_version: '01', value: '{}' },
    { id: 5, data_key: 'propieda#03', data_version: '01', value: '{}' },
    { id: 6, data_key: 'propieda#04', data_version: '01', value: '{}' },
    { id: 7, data_key: 'propieda#05', data_version: '01', value: '{}' },
    { id: 9, data_key: 'propieda#06', data_version: '01', value: '{}' },
    { id: 10, data_key: 'propieda#10', data_version: '01', value: '{}' },
    { id: 11, data_key: 'propieda#11', data_version: '01', value: '{}' },
    { id: 12, data_key: 'propieda#12', data_version: '01', value: '{}' },
  ];

  getData() {
    return this.data;
  }
}
