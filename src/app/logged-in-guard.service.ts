import { Injectable } from '@angular/core';
import { CanActivate, Router,
  ActivatedRouteSnapshot,
  CanActivateChild,
  RouterStateSnapshot,
  NavigationExtras,
  CanLoad, Route } from '@angular/router';

import { HttpService } from './@core/utils/http.service';

@Injectable()
export class LoggedInGuardService {

  constructor(private http: HttpService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return (this.http.popUser!=null);

  }

}
