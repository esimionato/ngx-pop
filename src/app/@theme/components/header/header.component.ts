import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { HttpService } from '../../../@core/utils/http.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  user: any;
  popUser: {};

  userMenu = [{ title: 'Mi Cuenta', link: '/auth/login' }, { title: 'Salir', link: '/auth/login' }];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              private userService: UserService,
              private analyticsService: AnalyticsService,
              private authService: NbAuthService,
              public http: HttpService) {

    this.authService.onTokenChange()
      .subscribe((token: NbAuthJWTToken) => {
        //console.log(" .: onTokenChange :. ");
        //console.log(token);
        if (token.getValue()) {
          //console.log(">> token: " + token.getValue());
          http.popUser = token.getPayload();
          this.popUser = http.popUser;
          this.http.headers.set('Authorization', ` Bearer ${token.getValue()}`);
          this.http.refreshGame();
        }
        //console.log(this.http);
        //console.log(this.popUser);
        //console.log(this.user);
      });
  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe((users: any) => this.user = users.walter);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }
}
