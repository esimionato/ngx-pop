import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { RemoteConfigModule } from './remote-config/remote-config.module';
import { VirtualCurrencyModule } from './virtual-currency/virtual-currency.module';
import { CatalogModule } from './catalog/catalog.module';
import { StatisticDefinitionModule } from './statistic-definition/statistic-definition.module';
import { EventDefinitionModule } from './event-definition/event-definition.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { LevellingCurveModule } from './levelling-curve/levelling-curve.module';
import { TournamentDefinitionModule } from './tournament-definition/tournament-definition.module';
import { AchievementDefinitionModule } from './achievement-definition/achievement-definition.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    StatisticDefinitionModule,
    EventDefinitionModule,
    LevellingCurveModule,
    TournamentDefinitionModule,
    AchievementDefinitionModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
})
export class PagesModule {
}
