import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualCurrencyComponent } from './virtual-currency.component';

describe('VirtualCurrencyComponent', () => {
  let component: VirtualCurrencyComponent;
  let fixture: ComponentFixture<VirtualCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
