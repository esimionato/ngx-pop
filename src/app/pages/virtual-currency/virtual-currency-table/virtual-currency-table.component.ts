import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';
import { VirtualCurrencySource } from './virtual-currency-source';

@Component({
  selector: 'virtual-currency-table',
  templateUrl: './virtual-currency-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class VirtualCurrencyTableComponent implements OnInit {

  settings = {
    pager: {
      display: true,
      perPage: 5,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      currencyIconUrl: {
        title: 'icono',
        type: 'html',
        width: '3%',
        valuePrepareFunction: (cell, row) => {
          return "<img src=\""+ row.currencyIconUrl+"\" class='img-avatar'/>";
        }
      },
      currencyCode: {
        title: 'Codigo',
        type: 'html',
        width: '2%',
        valuePrepareFunction: (cell, row) => {
          if(row.currencyCode!==undefined) {
            return "<span class='suptext2'>"+ row.currencyCode + "</span>";
          }
        }

      },
      currencyName: {
        title: 'Moneda',
        type: 'html',
        width: '15%',
        valuePrepareFunction: (cell, row) => {
          let r = row.currencyName;
          if(row.currencyBase==true) {
            r = r + " <sup class='suptext'> BASE</sup>";
          }
          return r;
        }
      },
      currencyBase: {
        title: 'Base ?',
        type: 'html',
        width: '9%',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.currencyBase) {
            return "SI";
          }
        }
      },
      currencyRatio: {
        title: 'Cotizacion',
        type: 'html',
        width: '10%',
        editor: { type: 'number' },
        filter: { type: 'number' },
        valuePrepareFunction: (cell, row) => {
          let r = '';
          if(row.currencyRatio!==undefined) {
            r = r + "<span class='suptext'>"+ row.currencyRatio +"</span>";
          }
          return r;
        }

      },
    initialDeposit: {
        title: 'Inicial',
        type: 'html',
        width: '10%',
        editor: { type: 'number' },
        filter: { type: 'number' },
        valuePrepareFunction: (cell, row) => {
          let r = '';
          if(row.initialDeposit!==undefined) {
            r = r + "<span class='suptext'>"+ row.initialDeposit +"</span>";
          }
          if(row.currencyCode!==undefined) {
            r = r + "<sup class='suptext2'>"+ row.currencyCode +"</sup>";
          }
          return r;
        }

    },
    rechargeRate: {
        title: 'Recarga',
        type: 'html',
        width: '10%',
        editor: { type: 'number' },
        filter: { type: 'number' },
        valuePrepareFunction: (cell, row) => {
          let r = '';
          if(row.rechargeRate!==undefined) {
            r = r + "<span class='suptext'>"+ row.rechargeRate +"</span>";
          }
          if(row.currencyCode!==undefined) {
            r = r + "<sup class='suptext2'>"+ row.currencyCode +"</sup>";
          }
          return r;
        }

    },
    maxLimit: {
        title: 'Tope',
        type: 'html',
        width: '10%',
        editor: { type: 'number' },
        filter: { type: 'number' },
        valuePrepareFunction: (cell, row) => {
          let r = '';
          if(row.maxLimit!==undefined) {
            r = r + "<span class='suptext'>"+ row.maxLimit +"</span>";
          }
          if(row.currencyCode!==undefined) {
            r = r + "<sup class='suptext2'>"+ row.currencyCode +"</sup>";
          }
          return r;
        }
    }
    }
  };

  source : VirtualCurrencySource;

  constructor(protected http: HttpService) {
    this.source = new VirtualCurrencySource(this.http);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.virtualCurrency+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
      //return this.http.doPut(this.http.paths.virtualCurrency+'/'+event.newData.id, event.newData).map(res => {
    return this.http.doPut(this.http.paths.virtualCurrency, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.virtualCurrency, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
