import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VirtualCurrencyTableComponent } from './virtual-currency-table.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [VirtualCurrencyTableComponent]
})
export class VirtualCurrencyTableModule { }
