import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VirtualCurrencyTableComponent } from './virtual-currency-table.component';

describe('VirtualCurrencyTableComponent', () => {
  let component: VirtualCurrencyTableComponent;
  let fixture: ComponentFixture<VirtualCurrencyTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VirtualCurrencyTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VirtualCurrencyTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
