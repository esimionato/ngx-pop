import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'virtual-currency',
  template: '<router-outlet></router-outlet>',
})

export class VirtualCurrencyComponent implements OnInit {

  constructor() { }
  ngOnInit() {
  }

}
