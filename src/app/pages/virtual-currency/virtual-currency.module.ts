import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';

import { ThemeModule } from '../../@theme/theme.module';

import { VirtualCurrencyComponent } from './virtual-currency.component';
import { VirtualCurrencyRoutingModule, routedComponents } from './virtual-currency-routing.module';
import { VirtualCurrencyTableComponent } from './virtual-currency-table/virtual-currency-table.component';

@NgModule({
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    HttpModule,
    ThemeModule,
    VirtualCurrencyRoutingModule
  ],
  declarations: [
    VirtualCurrencyComponent,
    VirtualCurrencyTableComponent
  ]
})
export class VirtualCurrencyModule {

}
