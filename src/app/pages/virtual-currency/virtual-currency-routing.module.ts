import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VirtualCurrencyComponent } from './virtual-currency.component';
import { VirtualCurrencyTableComponent } from './virtual-currency-table/virtual-currency-table.component';

const routes: Routes = [{
  path: '',
  component: VirtualCurrencyComponent,
  children: [{
    path: 'virtual-currency-table',
    component: VirtualCurrencyTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VirtualCurrencyRoutingModule { }

export const routedComponents = [
  VirtualCurrencyComponent,
  VirtualCurrencyTableComponent,
];
