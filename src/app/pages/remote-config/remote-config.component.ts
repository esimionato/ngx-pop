import { Component } from '@angular/core';

@Component({
  selector: 'remote-config',
  template: `<router-outlet></router-outlet>`,
})
export class RemoteConfigComponent {
}
