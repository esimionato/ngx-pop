import { Component } from '@angular/core';
import { RemoteConfigSource } from './remote-config-source';
import { HttpService } from '../../../@core/utils/http.service';

@Component({
  selector: 'remote-config-table',
  templateUrl: './remote-config-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class RemoteConfigTableComponent {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      dataKey: {
        title: 'Codigo',
        type: 'html',
        width: '20%',
        valuePrepareFunction: (cell, row) => {
          let r = row.dataKey;
          if(row.dataSpace !== undefined
            && row.dataSpace !== null
            && row.dataSpace !== "") {
            r = r + " <sup class='suptext'> " + row.dataSpace + "</sup>";
          }
          if(row.dataVersion !== undefined
            && row.dataVersion !== null
            && row.dataVersion !== "") {
            r = r + " <sup class='suptext2'>(v" + row.dataVersion + ")</sup>";
          }
          return r;
        }
      },
      scope: {
        title: 'scope',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'publico', value: 'PUBLIC'},
              {title: 'privado', value: 'PRIVATE'},
              {title: 'oculto', value: 'INTERNAL'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'publico', value: 'PUBLIC'},
              {title: 'privado', value: 'PRIVATE'},
              {title: 'oculto', value: 'INTERNAL'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.scope == "PUBLIC") {
            return "public";
          }
          if(row.scope == "PRIVATE") {
            return "privado";
          }
          if(row.scope == "INTERNAL") {
            return "oculto";
          }
        },
        width: '8%',
      },
      permissions: {
        title: 'permisos',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'escritura', value: 'WRITE'},
              {title: 'lectura', value: 'ONLY_READ'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'escritura', value: 'WRITE'},
              {title: 'lectura', value: 'ONLY_READ'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.permissions == "WRITE") {
            return "escritura";
          }
          if(row.permissions == "ONLY_READ") {
            return "lectura";
          }
        },
        width: '8%',
      },
      dataValue: {
        title: 'Valor',
        type: 'html',
        width: '70%',
        editor: {
          type: 'textarea',
        },
        filter: {
          type: 'textarea',
        },
        valuePrepareFunction: (cell, row) => {
            return "<span class='suptext3'> " + row.dataValue + "</span>";
        }
      },
      dataSpace: {
        title: 'Space',
        type: 'html',
        width: '15%',
        valuePrepareFunction: (cell, row) => {
          if(row.dataSpace !== undefined
            && row.dataSpace !== null
            && row.dataSpace !== "") {
            return "<span class='suptext'> " + row.dataSpace + "</span>";
          }
          return '';
        }
      }

    },
  };

  source: RemoteConfigSource;

  constructor(protected http: HttpService) {
    this.source = new RemoteConfigSource(this.http);
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event);
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.globalData+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.globalData, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.globalData, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
