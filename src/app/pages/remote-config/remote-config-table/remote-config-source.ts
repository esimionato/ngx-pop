import { Injectable } from '@angular/core';

import { HttpService } from '../../../@core/utils/http.service';
import { LocalDataSource } from 'ng2-smart-table';

@Injectable()
export class RemoteConfigSource extends LocalDataSource {

  lastRequestCount: number = 0;
  lastData: {};

  constructor(protected http: HttpService) {
    super();
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    return this.http.doGet(this.http.paths.globalData+'?sort=dataSpace&sort=dataKey').map(res => {
      return res.json();
    }).toPromise();
  }

  find(element: any): Promise<any> {
    return Promise.resolve(element);
  }


}
