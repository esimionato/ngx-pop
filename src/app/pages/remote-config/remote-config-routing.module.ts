import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RemoteConfigComponent } from './remote-config.component';
import { RemoteConfigTableComponent } from './remote-config-table/remote-config-table.component';

const routes: Routes = [{
  path: '',
  component: RemoteConfigComponent,
  children: [{
    path: 'remote-config-table',
    component: RemoteConfigTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RemoteConfigRoutingModule { }

export const routedComponents = [
  RemoteConfigComponent,
  RemoteConfigTableComponent,
];
