import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { HttpModule } from '@angular/http';

import { ThemeModule } from '../../@theme/theme.module';
import { RemoteConfigRoutingModule, routedComponents } from './remote-config-routing.module';

@NgModule({
  imports: [
    HttpModule,
    ThemeModule,
    RemoteConfigRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
  ],
})
export class RemoteConfigModule { }
