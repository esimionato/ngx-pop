import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'nb-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'CONFIGURACION',
    group: true,
  },
  {
    title: 'Perfil del Jugador',
    icon: 'nb-compose',
    children: [
      {
        title: 'Propiedades Jugador',
        link: '/pages/custom-data/custom-data-table',
      },
      {
        title: 'Estadisticas Jugador',
        link: '/pages/statistic-definition/statistic-definition-table',
      },
    ],
  },
  {
    title: 'Jugabilidad',
    icon: 'nb-gear',
    children: [
      {
        title: 'Configuracion Remota',
        link: '/pages/remote-config/remote-config-table',
      },
      {
        title: 'Eventos y Acciones',
        link: '/pages/event-definition/event-definition-table',
      },
      {
        title: 'Curva de Nivel XP',
        link: '/pages/levelling-curve/levelling-curve-table',
      },
      {
        title: 'Logros y Recompensas',
        link: '/pages/achievement-definition/achievement-definition-table',
      },
      {
        title: 'Torneos y Rankings',
        link: '/pages/tournament-definition/tournament-definition-table',
      },
    ],
  },
  {
    title: 'Monetizacion',
    icon: 'ion ion-social-usd-outline',
    children: [
      {
        title: 'Economia',
        link: '/pages/virtual-currency/virtual-currency-table',
      }, {
        title: 'Catalogos',
        link: '/pages/catalog/catalog-item-table',
      },
    ],
  },
  {
    title: 'MANAGEMENT',
    group: true,
  },
  {
    title: 'Jugadores',
    icon: 'ion ion-ios-people-outline',
    link: '/pages/players/players-table',
  },
  {
    title: 'Metricas',
    icon: 'nb-bar-chart',
    link: '/pages/player/profile',
  },
  {
    title: 'Monitoreo',
    icon: 'ion ion-speedometer',
  },
  {
    title: '__________________',
    group: true,
  },
  {
    title: 'Opciones',
    icon: 'ion ion-settings',
  },
  {
    title: 'Ayuda',
    icon: 'ion ion-ios-help-outline',
  },

];
