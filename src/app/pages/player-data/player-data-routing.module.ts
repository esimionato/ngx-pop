import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayerDataComponent } from './player-data.component';
import { PlayerDataTableComponent } from './player-data-table/player-data-table.component';

const routes: Routes = [{
  path: '',
  component: PlayerDataComponent,
  children: [{
    path: 'player-data-table',
    component: PlayerDataTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayerDataRoutingModule { }

export const routedComponents = [
  PlayerDataComponent,
  PlayerDataTableComponent,
];
