import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

import { PlayerDataService } from '../../../@core/data/player-data.service';

@Component({
  selector: 'pop-player-data-table',
  templateUrl: './player-data-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class PlayerDataTableComponent {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      data_key: {
        title: 'codigo',
        type: 'string',
        editable: false,
        width: '20%',
      },
      data_version: {
        title: 'version',
        type: 'number',
        editable: false,
        width: '1%',
      },
      value: {
        title: 'valor',
        type: 'textarea',
        width: '70%',
        editor: {
          type: 'textarea',
        },
        filter: {
          type: 'textarea',
        },
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(private service: PlayerDataService) {
    const data = this.service.getData();
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
