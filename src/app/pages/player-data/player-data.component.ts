import { Component } from '@angular/core';

@Component({
  selector: 'player-data',
  template: `<router-outlet></router-outlet>`,
})
export class PlayerDataComponent {
}
