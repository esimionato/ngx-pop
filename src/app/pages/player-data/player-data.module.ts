import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { HttpModule } from '@angular/http';

import { ThemeModule } from '../../@theme/theme.module';
import { PlayerDataRoutingModule, routedComponents } from './player-data-routing.module';
import { PlayerDataService } from '../../@core/data/player-data.service';

@NgModule({
  imports: [
    HttpModule,
    ThemeModule,
    PlayerDataRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PlayerDataService,
  ],
})
export class PlayerDataModule  {

}
