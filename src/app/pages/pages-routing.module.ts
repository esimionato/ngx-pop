import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [{
    path: 'dashboard',
    component: DashboardComponent,
  }, {
    path: 'ui-features',
    loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
  }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  }, {
    path: 'custom-data',
    loadChildren: './custom-data/custom-data.module#CustomDataModule',
  }, {
    path: 'remote-config',
    loadChildren: './remote-config/remote-config.module#RemoteConfigModule',
  }, {
    path: 'players',
    loadChildren: './players/players.module#PlayersModule',
  }, {
    path: 'player',
    loadChildren: './player/player.module#PlayerModule',
  }, {
    path: 'player-data',
    loadChildren: './player-data/player-data.module#PlayerDataModule',
  }, {
    path: 'virtual-currency',
    loadChildren: './virtual-currency/virtual-currency.module#VirtualCurrencyModule',
  }, {
    path: 'catalog',
    loadChildren: './catalog/catalog.module#CatalogModule',
  }, {
    path: 'statistic-definition',
    loadChildren: './statistic-definition/statistic-definition.module#StatisticDefinitionModule',
  }, {
    path: 'event-definition',
    loadChildren: './event-definition/event-definition.module#EventDefinitionModule',
  }, {
    path: 'levelling-curve',
    loadChildren: './levelling-curve/levelling-curve.module#LevellingCurveModule',
  }, {
    path: 'achievement-definition',
    loadChildren: './achievement-definition/achievement-definition.module#AchievementDefinitionModule',
  }, {
    path: 'tournament-definition',
    loadChildren: './tournament-definition/tournament-definition.module#TournamentDefinitionModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
