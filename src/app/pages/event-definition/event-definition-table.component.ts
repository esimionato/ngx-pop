import { Component, OnInit } from '@angular/core';

import { HttpService } from '../../@core/utils/http.service';

import { EventDefinitionSource } from './event-definition-source';
import { StatisticDefinitionSource } from '../statistic-definition/statistic-definition-table/statistic-definition-source';

@Component({
  selector: 'app-event-definition-table',
  templateUrl: './event-definition-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class EventDefinitionTableComponent implements OnInit {

  statisticList = [];
  statisticArr: { [index: string]: any; } = {};
  statisticRow: Object;
  settings: Object;
  allstatistics: Object;

  source : EventDefinitionSource;
  statisticSource : StatisticDefinitionSource;

  constructor(protected http: HttpService) {
    this.source = new EventDefinitionSource(this.http);
    this.statisticSource = new StatisticDefinitionSource(this.http);
    this.allstatistics = this.http.doGet(this.http.paths.statisticDefinition).map(res => {
      res.json().forEach(statistic => {
       console.log(statistic);
       this.statisticList.push({value: statistic.id, title: statistic.stCode});
       this.statisticArr[statistic.id] = statistic;
      });
      this.settings = this.loadSettings();
      return res.json();
    }).toPromise();
  }


  ngOnInit() {
  }

  loadSettings() {
    return {
      pager: {
        display: true,
        perPage: 10,
      },
      add: {
        confirmCreate: true,
        addButtonContent: '<i class="nb-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      edit: {
        confirmSave: true,
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
      },
      delete: {
        confirmDelete: true,
        deleteButtonContent: '<i class="nb-trash"></i>',
      },
      columns: {
        evCode: {
          title: 'Codigo',
          type: 'string',
          width: '10%'
        },
        evName: {
          title: 'Evento',
          type: 'string',
          width: '20%',
        },
        evType: {
          title: 'Tipo de Evento',
          type: 'string',
          width: '8',
          editor: {
            type: 'list',
            config: {
              list: [
                {title: 'estadistico', value: 'STATISTIC'},
                {title: 'datalog', value: 'DATALOG'}
              ]
            }
          },
          filter: {
            type: 'list',
            config: {
              list: [
                {title: 'estadistico', value: 'STATISTIC'},
                {title: 'datalog', value: 'DATALOG'}
              ]
            }
          }
        },
        statistics: {
          title: 'Estadisticas',
          type: 'html',
          width: '20%',
          valuePrepareFunction: (cell, row) => {
            var text = "<span>";
            if(row.evType=="STATISTIC") {
              for (var i = 0; i < row.statistics.length; i++) {
                text += row.statistics[i].stCode + " ";
              }
            }
            return text + "</span>";
          },
          editor: {
            type: 'list',
            config: {
              list: this.statisticList
            }
          },
          filter: {
            type: 'list',
            config: {
              list: this.statisticList
            }
          }
        }
      }
  };
  }




  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.eventDefinition+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    let stList = [];
    if(event.newData.evType=="STATISTIC") {
      stList.push(this.statisticArr[event.newData.statistics]);
    }
    event.newData.statistics = stList;
    return this.http.doPut(this.http.paths.eventDefinition, event.newData).map(res => {
      this.settings = this.loadSettings();
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    let stList = [];
    if(event.newData.evType=="STATISTIC") {
      stList.push(this.statisticArr[event.newData.statistics]);
    }
    event.newData.statistics = stList;
    return this.http.doPost(this.http.paths.eventDefinition, event.newData).map(res => {
      this.settings = this.loadSettings();
      return res.json();
    }).toPromise();
  }

}
