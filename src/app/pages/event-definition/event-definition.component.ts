import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-definition',
  template: '<router-outlet></router-outlet>'
})
export class EventDefinitionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
