import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';

import { EventDefinitionComponent } from './event-definition.component';
import { EventDefinitionRoutingModule } from './event-definition-routing.module';
import { EventDefinitionTableComponent } from './event-definition-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ThemeModule,
    Ng2SmartTableModule,
    EventDefinitionRoutingModule
  ],
  declarations: [
    EventDefinitionComponent,
    EventDefinitionTableComponent
  ]
})
export class EventDefinitionModule { }
