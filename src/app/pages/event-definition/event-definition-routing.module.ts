import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventDefinitionComponent } from './event-definition.component';
import { EventDefinitionTableComponent } from './event-definition-table.component';

const routes: Routes = [{
  path: '',
  component: EventDefinitionComponent,
  children: [{
    path: 'event-definition-table',
    component: EventDefinitionTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventDefinitionRoutingModule { }

export const routedComponents = [
  EventDefinitionTableComponent,
]
