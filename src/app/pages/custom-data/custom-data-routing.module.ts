import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomDataComponent } from './custom-data.component';
import { CustomDataTableComponent } from './custom-data-table/custom-data-table.component';

const routes: Routes = [{
  path: '',
  component: CustomDataComponent,
  children: [{
    path: 'custom-data-table',
    component: CustomDataTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomDataRoutingModule { }

export const routedComponents = [
  CustomDataComponent,
  CustomDataTableComponent,
];
