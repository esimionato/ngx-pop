import { Injectable } from '@angular/core';

import { HttpService } from '../../../@core/utils/http.service';
import { LocalDataSource } from 'ng2-smart-table';

@Injectable()
export class CustomDataSource extends LocalDataSource {

  lastRequestCount: number = 0;
  lastData: {};

  constructor(protected http: HttpService) {
    super();
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    //    if (this.sortConf) {
    //      this.sortConf.forEach((fieldConf) => {
    //        url += `_sort=${fieldConf.field}&_order=${fieldConf.direction.toUpperCase()}&`;
    //      });
    //    }
    //
    //    if(this.pagingConf && this.pagingConf['page'] && this.pagingConf['perPage']) {
    //      url += `_page=${this.pagingConf['page']}_like=${fieldConf['search']}&`;
    //    }
    //
    //    if(this.pagingConf.filters) {
    //      this.filterConf.filters.forEach((fieldConf) => {
    //        if (fieldConf['search']) {
    //          url += `${fieldConf['field']}_like=${fieldConf['search']}&`;
    //        }
    //      });
    //    }

    return this.http.doGet(this.http.paths.customData+"?sort=dataKlass").map(res => {
      //this.lastRequestCount = +res.headers.get('x-total-count');
      return res.json();
    }).toPromise();
  }

    /*
  //public update(element: Trigger, values: Trigger): Promise<any> {
  update(element: any, values: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.find(element).then((found) => {

        element.name = values.name;
        element.enabled = values.enabled;
        element.condition = values.condition;

        found = deepExtend(found, values);
        super.update(found, values).then(resolve).catch(reject);
        this.emitOnUpdated(element);
        this.emitOnChanged(element);
        resolve();
      }).catch(reject);
    });
  }
     */

  find(element: any): Promise<any> {
    //const found = this.data.find(el => el === element);
    //if (found) {
    //  return Promise.resolve(found);
    //}

    return Promise.resolve(element);
    //return Promise.reject(new Error('Element was not found in the dataset'));
  }


}
