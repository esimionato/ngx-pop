import { Component } from '@angular/core';
//import { LocalDataSource } from 'ng2-smart-table';
//import { ServerDataSource } from 'ng2-smart-table';
import { CustomDataService } from '../../../@core/data/custom-data.service';
import { CustomDataSource } from './custom-data-source';
import { HttpService } from '../../../@core/utils/http.service';

@Component({
  selector: 'custom-data-table',
  templateUrl: './custom-data-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class CustomDataTableComponent {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      dataKlass: {
        title: 'Clase',
        type: 'html',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'skill', value: 'SKILL'},
              {title: 'metadata', value: 'METADATA'},
              {title: 'progression', value: 'PROGRESSION'},
              {title: 'perfil', value: 'PERFIL'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'Skill', value: 'SKILL'},
              {title: 'Metadata', value: 'METADATA'},
              {title: 'Progression', value: 'PROGRESSION'},
              {title: 'Perfil', value: 'PERFIL'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          let r = "<span class='suptext2'>";
          if(row.dataKlass == "PROGRESION") {
            r = r +"xp";
          }
          if(row.dataKlass == "METADATA") {
            r = r + "meta";
          }
          if(row.dataKlass == "SKILL") {
            r = r + "skill";
          }
          if(row.dataKlass == "PERFIL") {
            r = r + "perfil";
          }
          r = r + '</span>';
          return r;
        },
        width: '2%',
      },
      dataIconUrl: {
        title: 'Icono',
        type: 'html',
        width: '3%',
        valuePrepareFunction: (cell, row) => {
          if(row.dataIconUrl === null|| row.dataIconUrl == "") {
            return "";
          } else {
            return "<img src=\""+ row.dataIconUrl+"\" class='img-avatar'/>";
          }
        }
      },
      dataKey: {
        title: 'Codigo',
        type: 'html',
        width: '15%',
        valuePrepareFunction: (cell, row) => {
          let r = row.dataKey;

          if(row.dataKlass !== undefined
            && row.dataKlass !== null
            && row.dataKlass !== "") {

            r = r +" <sup class='suptext2'>";
            if(row.dataKlass == "PROGRESION") {
              r = r +"xp";
            }
            if(row.dataKlass == "METADATA") {
              r = r + "meta";
            }
            if(row.dataKlass == "SKILL") {
              r = r + "skill";
            }
            if(row.dataKlass == "PERFIL") {
              r = r + "perfil";
            }
            r = r + '</sup>';
          }

          if(row.dataSpace !== undefined
            && row.dataSpace !== null
            && row.dataSpace !== "") {
            r = r + " <sup class='suptext'> " + row.dataSpace + "</sup>";
          }

          return r;
        }
      },
      dataName: {
        title: 'Propiedad',
        type: 'string',
        width: '25%'
      },
      scope: {
        title: 'scope',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'publico', value: 'PUBLIC'},
              {title: 'privado', value: 'PRIVATE'},
              {title: 'oculto', value: 'INTERNAL'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'publico', value: 'PUBLIC'},
              {title: 'privado', value: 'PRIVATE'},
              {title: 'oculto', value: 'INTERNAL'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.scope == "PUBLIC") {
            return "public";
          }
          if(row.scope == "PRIVATE") {
            return "privado";
          }
          if(row.scope == "INTERNAL") {
            return "oculto";
          }
        },
        width: '6%',
      },
      permissions: {
        title: 'Permisos',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'escritura', value: 'WRITE'},
              {title: 'lectura', value: 'ONLY_READ'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'escritura', value: 'WRITE'},
              {title: 'lectura', value: 'ONLY_READ'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.permissions == "WRITE") {
            return "escritura";
          }
          if(row.permissions == "ONLY_READ") {
            return "lectura";
          }
        },
        width: '6%',
      },
      fixed: {
        title: 'Irreversible',
        width: '3%',
       editor: {
          type: 'list',
          config: {
            list: [
              {title: 'SI', value: 'true'},
              {title: 'NO', value: 'false'},
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'SI', value: 'true'},
              {title: 'NO', value: 'false'},
            ]
          }
        },
        valuePrepareFunction: (cell, row) => {
          if(row.fixed) {
            return "SI";
          }
        }
      },
      defaultValue: {
        title: 'Default',
        type: 'html',
        width: '70%',
        editor: {
          type: 'textarea',
        },
        filter: {
          type: 'textarea',
        },
        valuePrepareFunction: (cell, row) => {
          if(row.defaultValue !== undefined
            && row.defaultValue !== null
            && row.defaultValue !== "") {
            return "<span class='suptext3'> " + row.defaultValue + "</span>";
          }
        }

      },
      dataSpace: {
        title: 'Space',
        type: 'html',
        width: '10%',
        valuePrepareFunction: (cell, row) => {
          if(row.dataSpace !== undefined
            && row.dataSpace !== null
            && row.dataSpace !== "") {
            return "<span class='suptext'> " + row.dataSpace + "</span>";
          }
          return '';
        }
      }
    },
  };

  source: CustomDataSource;

  constructor(private service: CustomDataService, protected http: HttpService) {
    this.source = new CustomDataSource(this.http);
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event);
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.customData+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.customData, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.customData, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
