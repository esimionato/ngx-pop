import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { HttpModule } from '@angular/http';

import { ThemeModule } from '../../@theme/theme.module';
import { CustomDataRoutingModule, routedComponents } from './custom-data-routing.module';
import { CustomDataService } from '../../@core/data/custom-data.service';

@NgModule({
  imports: [
    HttpModule,
    ThemeModule,
    CustomDataRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    CustomDataService,
  ],
})
export class CustomDataModule { }
