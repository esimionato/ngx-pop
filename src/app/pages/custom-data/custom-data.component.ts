import { Component } from '@angular/core';

@Component({
  selector: 'custom-data',
  template: `<router-outlet></router-outlet>`,
})
export class CustomDataComponent {
}
