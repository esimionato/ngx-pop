import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';

import { LevellingCurveSource } from '../../levelling-curve/levelling-curve-table/levelling-curve-source';
import { AchievementDefinitionSource } from './achievement-definition-source';

@Component({
  selector: 'achievement-definition-table',
  templateUrl: './achievement-definition-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
    .img-achievement {
      max-height: 3.25rem;
      border: solid 2px #00f9a6;
    }
  `],
})
export class AchievementDefinitionTableComponent implements OnInit {

  settings: any = {};

  source : AchievementDefinitionSource;
  levels = [];
	levelsArr: { [index: string]: any; } = {};

  constructor(protected http: HttpService) {
    this.source = new AchievementDefinitionSource(this.http);
		this.http.doGet(this.http.paths.levellingCurve).map(res => {
			res.json().forEach(level => {
				this.levels.push({value: level.lvCode, title: level.lvCode});
				this.levelsArr[level.lvCode] = level;
		});
		this.settings = this.loadSettings();
		return res.json();
  }).toPromise();
	}

	loadSettings() {
		return {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
    avIconUrl: {
      title: 'Icono',
      type: 'html',
      width: '10%',
      valuePrepareFunction: (cell, row) => {
        return "<img src=\""+ row.avIconUrl+"\" class='img-achievement' style='border-radius:0%; max-width: fit-content;'/>";
      }
    },
    avCode: {
      title: 'Codigo',
      type: 'string',
      width: '10%'
    },
    avTitle: {
      title: 'Logro',
      type: 'string',
      width: '10%',
    },
      avType: {
			title: 'Tipo de Logro',
      type: 'string',
      width: '10',
      editor: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'NIVEL_LOGRADO', value: 'LEVEL_BASE'},
            {title: 'EVENTO_LOGRADO', value: 'EVENt_BASE'},
            {title: 'STATISTICA_SUPERADA', value: 'STATS_BASE'},
            {title: 'TORNEO_GANADO', value: 'TOURNAMENT_BASE'}
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'NIVEL_LOGRADO', value: 'LEVEL_BASE'},
            //{title: 'EVENTO_LOGRADO', value: 'EVENt_BASE'},
            //{title: 'STATISTICA_SUPERADA', value: 'STATS_BASE'},
            {title: 'TORNEO_GANADO', value: 'TOURNAMENT_BASE'}
          ]
        }
      }
    },
		goalLevelLvCode: {title: 'Nivel Logrado',
          type: 'html',
          width: '10%',
          valuePrepareFunction: (cell, row) => {
            var text = "";
            if(row.avType=="LEVEL_BASE") {
        			text += "<img src=\""+ this.levelsArr[row.goalLevelLvCode].lvIconUrl+"\" class='img-avatar' style='border-radius:0%; max-width: fit-content;'/>";
              text += "<br/><span>"+row.goalLevelLvCode + "</span> ";
            }
            return text + "";
          },
          editor: {
            type: 'list',
            config: {
              list: this.levels
            }
          },
          filter: {
            type: 'list',
            config: {
              list: this.levels
            }
          }
        },
		goalTournament: {title: 'Torneo Ganado'},
		rewardMessage: {title: 'Descripcion del Logro'}
    }
  };

	}

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.achievementDefinition+'/'+event.data.id).map(res => {
			this.loadSettings();
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    if(event.newData.avType=="LEVEL_BASE") {
    	event.newData.goalLevelId = this.levelsArr[event.newData.goalLevelLvCode].id;
    }
    return this.http.doPut(this.http.paths.achievementDefinition, event.newData).map(res => {
			this.loadSettings();
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    if(event.newData.avType=="LEVEL_BASE") {
    	event.newData.goalLevelId = this.levelsArr[event.newData.goalLevelLvCode].id;
    }
    return this.http.doPost(this.http.paths.achievementDefinition, event.newData).map(res => {
			this.loadSettings();
      return res.json();
    }).toPromise();
  }

}
