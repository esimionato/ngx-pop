import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementDefinitionTableComponent } from './achievement-definition-table.component';

describe('AchievementDefinitionTableComponent', () => {
  let component: AchievementDefinitionTableComponent;
  let fixture: ComponentFixture<AchievementDefinitionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementDefinitionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementDefinitionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
