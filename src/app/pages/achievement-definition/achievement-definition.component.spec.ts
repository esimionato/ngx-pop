import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AchievementDefinitionComponent } from './achievement-definition.component';

describe('AchievementDefinitionComponent', () => {
  let component: AchievementDefinitionComponent;
  let fixture: ComponentFixture<AchievementDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AchievementDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AchievementDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
