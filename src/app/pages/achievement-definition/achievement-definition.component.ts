import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'achievement-definition',
  template: '<router-outlet></router-outlet>',
  styles: []
})
export class AchievementDefinitionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
