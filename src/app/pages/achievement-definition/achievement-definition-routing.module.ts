import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AchievementDefinitionComponent } from './achievement-definition.component';
import { AchievementDefinitionTableComponent } from './achievement-definition-table/achievement-definition-table.component';

const routes: Routes = [{
  path: '',
  component: AchievementDefinitionComponent,
  children: [{
    path: 'achievement-definition-table',
    component: AchievementDefinitionTableComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AchievementDefinitionRoutingModule { }

export const routedComponents = [
  AchievementDefinitionTableComponent
]
