import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';


import { AchievementDefinitionRoutingModule } from './achievement-definition-routing.module';
import { AchievementDefinitionComponent } from './achievement-definition.component';
import { AchievementDefinitionTableComponent } from './achievement-definition-table/achievement-definition-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ThemeModule,
    Ng2SmartTableModule,
    AchievementDefinitionRoutingModule
  ],
  declarations: [AchievementDefinitionComponent, AchievementDefinitionTableComponent]
})
export class AchievementDefinitionModule { }
