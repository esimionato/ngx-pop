import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';

import { LevellingCurveRoutingModule } from './levelling-curve-routing.module';
import { LevellingCurveComponent } from './levelling-curve.component';
import { LevellingCurveTableComponent } from './levelling-curve-table/levelling-curve-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ThemeModule,
    Ng2SmartTableModule,
    LevellingCurveRoutingModule
  ],
  declarations: [LevellingCurveComponent, LevellingCurveTableComponent]
})
export class LevellingCurveModule { }
