import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevellingCurveComponent } from './levelling-curve.component';

describe('LevellingCurveComponent', () => {
  let component: LevellingCurveComponent;
  let fixture: ComponentFixture<LevellingCurveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevellingCurveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevellingCurveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
