import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LevellingCurveComponent } from './levelling-curve.component';
import { LevellingCurveTableComponent } from './levelling-curve-table/levelling-curve-table.component';

const routes: Routes = [{
  path: '',
  component: LevellingCurveComponent,
  children: [{
    path: 'levelling-curve-table',
    component: LevellingCurveTableComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LevellingCurveRoutingModule { }

export const routedComponents = [
  LevellingCurveTableComponent
]
