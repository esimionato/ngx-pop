import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'levelling-curve',
  template: '<router-outlet></router-outlet>',
  styles: []
})
export class LevellingCurveComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
