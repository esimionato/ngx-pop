import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevellingCurveTableComponent } from './levelling-curve-table.component';

describe('LevellingCurveTableComponent', () => {
  let component: LevellingCurveTableComponent;
  let fixture: ComponentFixture<LevellingCurveTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevellingCurveTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevellingCurveTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
