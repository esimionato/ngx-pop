import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';
import { LevellingCurveSource } from './levelling-curve-source';

@Component({
  selector: 'levelling-curve-table',
  templateUrl: './levelling-curve-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class LevellingCurveTableComponent implements OnInit {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      lvNumber: {
        title: '#',
        type: 'number',
        width: '2%'
      },
    lvIconUrl: {
      title: 'Icono',
      type: 'html',
      width: '10%',
      valuePrepareFunction: (cell, row) => {
        return "<img src=\""+ row.lvIconUrl+"\" class='img-avatar' style='border-radius:0%; max-width: fit-content;'/>";
      }
    },
    lvCode: {
      title: 'Codigo',
      type: 'string',
      width: '10%'
    },
    lvDescription: { },
    xpBase: {
      title: 'XP'
    }
    }
  };

  source : LevellingCurveSource;

  constructor(protected http: HttpService) {
    this.source = new LevellingCurveSource(this.http);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.levellingCurve+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.levellingCurve, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.levellingCurve, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
