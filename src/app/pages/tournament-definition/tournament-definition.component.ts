import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tournament-definition',
  template: '<router-outlet></router-outlet>',
  styles: []
})
export class TournamentDefinitionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
