import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentDefinitionComponent } from './tournament-definition.component';

describe('TournamentDefinitionComponent', () => {
  let component: TournamentDefinitionComponent;
  let fixture: ComponentFixture<TournamentDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
