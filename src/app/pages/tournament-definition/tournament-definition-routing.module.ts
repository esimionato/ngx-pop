import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TournamentDefinitionComponent } from './tournament-definition.component';
import { TournamentDefinitionTableComponent } from './tournament-definition-table/tournament-definition-table.component';

const routes: Routes = [{
  path: '',
  component: TournamentDefinitionComponent,
  children: [{
    path: 'tournament-definition-table',
    component: TournamentDefinitionTableComponent,
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TournamentDefinitionRoutingModule { }

export const routedComponents = [
  TournamentDefinitionTableComponent,
]
