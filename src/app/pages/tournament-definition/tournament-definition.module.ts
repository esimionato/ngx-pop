import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';

import { TournamentDefinitionRoutingModule } from './tournament-definition-routing.module';
import { TournamentDefinitionComponent } from './tournament-definition.component';
import { TournamentDefinitionTableComponent } from './tournament-definition-table/tournament-definition-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ThemeModule,
    Ng2SmartTableModule,
    TournamentDefinitionRoutingModule
  ],
  declarations: [TournamentDefinitionComponent, TournamentDefinitionTableComponent]
})
export class TournamentDefinitionModule { }
