import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TournamentDefinitionTableComponent } from './tournament-definition-table.component';

describe('TournamentDefinitionTableComponent', () => {
  let component: TournamentDefinitionTableComponent;
  let fixture: ComponentFixture<TournamentDefinitionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TournamentDefinitionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TournamentDefinitionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
