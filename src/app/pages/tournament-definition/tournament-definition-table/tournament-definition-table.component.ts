import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';
import { TournamentDefinitionSource } from './tournament-definition-source';


@Component({
  selector: 'tournament-definition-table',
  templateUrl: './tournament-definition-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class TournamentDefinitionTableComponent implements OnInit {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
    tnIconUrl: {
      title: 'Icono',
      type: 'html',
      width: '10%',
      valuePrepareFunction: (cell, row) => {
        return "<img src=\""+ row.tnIconUrl+"\" class='img-achievement' style='border-radius:0%; max-width: fit-content;'/>";
      }
    },
    tnCode: {
      title: 'Codigo',
      type: 'string',
      width: '10%'
    },
    tnTitle: {
      title: 'Torneo',
      type: 'string',
      width: '20%',
    },
    tnType: {
      title: 'Tipo',
      type: 'string',
      width: '5%',
    },
    status: {
      title: 'Estado',
      type: 'string',
      width: '10%',
      editable: false,
      editor: {
        type: 'list',
        config: {
          list: [
            {title: 'EnCurso', value: 'ACTIVE'},
            {title: 'Finalizado', value: 'CLOSED'},
            {title: 'Pendiente', value: 'DRAFT'}
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: 'en curso', value: 'ACTIVE'},
            {title: 'finalizado', value: 'CLOSED'},
            {title: 'pendiente', value: 'DRAFT'}
          ]
        }
      }
    },
    resetFrecuency: {
      title: 'Repeticion',
      type: 'string',
      width: '10%',
      editor: {
        type: 'list',
        config: {
          list: [
            {title: 'manual', value: 'MANUALLY'},
            {title: 'semanal', value: 'WEEKLY'},
            {title: 'mensual', value: 'MONTHLY'}
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: 'manual', value: 'MANUALLY'},
            {title: 'semanal', value: 'WEEKLY'},
            {title: 'mensual', value: 'MONTHLY'}
          ]
        }
      }
    },
    joinType: {
      title: 'Inscripcion',
      type: 'string',
      width: '10%',
      editor: {
        type: 'list',
        config: {
          list: [
            {title: 'automatico', value: 'AUTOMATIC'},
            {title: 'libre', value: 'FREE'},
            {title: 'pago', value: 'PAID'}
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: 'automatico', value: 'AUTOMATIC'},
            {title: 'libre', value: 'FREE'},
            {title: 'pago', value: 'PAID'}
          ]
        }
      }
    },
    startedAt: {
    title: 'Inicio'
    }
   // goalStatistic: {
   //   title: '',
   //   type: 'string',
   //   width: '5%',
   // }
    }
  };

  source : TournamentDefinitionSource;

  constructor(protected http: HttpService) {
    this.source = new TournamentDefinitionSource(this.http);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.tournamentDefinition+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.tournamentDefinition, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.tournamentDefinition, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
