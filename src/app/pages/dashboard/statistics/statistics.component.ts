import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';

import { StatisticsService } from '../../../@core/data/statistics.service';

@Component({
  selector: 'ngx-statistics',
  styleUrls: ['./statistics.component.scss'],
  templateUrl: './statistics.component.html',
})
export class StatisticsComponent implements OnDestroy {

  data: Array<any>;

  type = 'week';
  types = ['week', 'month', 'year'];

  currentTheme: string;
  themeSubscription: any;

  constructor(private eService: StatisticsService, private themeService: NbThemeService) {
    this.data = eService.getData();

    this.themeSubscription = this.themeService.getJsTheme().subscribe(theme => {
      this.currentTheme = theme.name;
    });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}
