import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbThemeService, NbMediaBreakpoint, NbMediaBreakpointsService } from '@nebular/theme';

import { PlayersService } from '../../../@core/data/players.service';

@Component({
  selector: 'ngx-players',
  styleUrls: ['./players.component.scss'],
  templateUrl: './players.component.html',
})
export class PlayersComponent implements OnInit, OnDestroy {

  players: any[];
  recent: any[];
  breakpoint: NbMediaBreakpoint;
  breakpoints: any;
  themeSubscription: any;

  constructor(private playersService: PlayersService,
              private themeService: NbThemeService,
              private breakpointService: NbMediaBreakpointsService) {

    this.breakpoints = breakpointService.getBreakpointsMap();
    this.themeSubscription = themeService.onMediaQueryChange()
      .subscribe(([oldValue, newValue]) => {
        this.breakpoint = newValue;
      });
  }

  ngOnInit() {

    this.playersService.getPlayers()
      .subscribe((players: any) => {
        this.players = [
          {player: players.P001, type: players.P001.pop_id},
          {player: players.P002, type: players.P002.pop_id},
          {player: players.P003, type: players.P003.pop_id},
          {player: players.P004, type: players.P004.pop_id},
          {player: players.P005, type: players.P005.pop_id},
          {player: players.P006, type: players.P005.pop_id},
          {player: players.P007, type: players.P005.pop_id},
          {player: players.P008, type: players.P005.pop_id},
          {player: players.P009, type: players.P001.pop_id},
          {player: players.P010, type: players.P002.pop_id},
          {player: players.P001, type: players.P003.pop_id},
          {player: players.P002, type: players.P004.pop_id},
          {player: players.P003, type: players.P005.pop_id},
        ];

        this.recent = [

          {player: players.P010, type: players.P002.pop_id},
          {player: players.P009, type: players.P001.pop_id},
          {player: players.P008, type: players.P005.pop_id},
          {player: players.P007, type: players.P005.pop_id},
          {player: players.P006, type: players.P005.pop_id},
          {player: players.P005, type: players.P005.pop_id},
          {player: players.P004, type: players.P004.pop_id},
          {player: players.P003, type: players.P003.pop_id},
          {player: players.P002, type: players.P002.pop_id},
          {player: players.P001, type: players.P001.pop_id},
          {player: players.P010, type: players.P002.pop_id},
          {player: players.P009, type: players.P001.pop_id},
          {player: players.P008, type: players.P005.pop_id},
        ];
      });
  }

  ngOnDestroy() {
    this.themeSubscription.unsubscribe();
  }
}
