import { Component } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';

//import { PlayerDataService } from '../../../@core/data/player-data.service';

@Component({
  selector: 'pop-player-economy-table',
  templateUrl: './player-economy-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class PlayerEconomyTableComponent {

  settings = {
    hideHeader: 'false',
    hideSubHeader: 'false',
    filter: false,
    actions: {
      add: false,
      delete: false,
      position: 'right',
      columnTitle: ''
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    columns: {
      currency_name: {
        title: '',
        class: 'player-profile-pop-id',
        filter: false,
        type: 'string',
        editable: false,
        width: '40%',
      },
      currency_simbol: {
        title: '',
        class: 'player-profile-pop-id',
        filter: false,
        type: 'string',
        editable: false,
        width: '20px',
      },
      amount: {
        title: 'Saldo',
        filter: false,
        type: 'number',
        width: '20%',
        editor: {
          type: 'number',
        },
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  //constructor(private service: PlayerDataService) {
  constructor() {
    ///const data = this.service.getData();
    const data = [
      {
        currency_simbol: 'CN',
        currency_name: 'Monedas',
        amount: '540'
      },
      {
        currency_simbol: 'GD',
        currency_name: 'Oro',
        amount: '80'
      },
      {
        currency_simbol: 'GM',
        currency_name: 'Gemas',
        amount: '20'
      },
      {
        currency_simbol: 'LF',
        currency_name: 'Vidas',
        amount: '10'
      },
      {
        currency_simbol: 'HE',
        currency_name: 'Corazones',
        amount: '5'
      },

    ];
    this.source.load(data);
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }
}
