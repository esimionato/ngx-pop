import { Component } from '@angular/core';

import { PlayerDataTableComponent } from './../player-data-table/player-data-table.component';

@Component({
  selector: 'pop-player-profile',
  styleUrls: ['./player-profile.component.scss'],
  templateUrl: './player-profile.component.html',
  // directives: [PlayerDataTableComponent]
})
export class PlayerProfileComponent {

  player = {
    pop_id: "g2c7c0042511c086",
    username: 'noe139',
    avatar_url: 'assets/images/avatars/players/025',
    session: '01:00',
    time: '09:12 pm',
    device_id: "afdsfadfa",
    status: 'banned',
    online: 'false',
    created: 'today',
    lastlogin: '8:30 am'
  };
}
