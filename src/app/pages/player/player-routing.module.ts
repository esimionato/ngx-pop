import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayerComponent } from './player.component';
import { PlayerProfileComponent } from './player-profile/player-profile.component';

const routes: Routes = [{
  path: '',
  component: PlayerComponent,
  children: [
    {
     path: 'profile',
     component: PlayerProfileComponent,
    }
  ],
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class PlayerRoutingModule {

}

export const routedComponents = [
  PlayerComponent,
  PlayerProfileComponent,
];
