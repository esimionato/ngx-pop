import { Component } from '@angular/core';

import {
  NbMenuItem,
} from '@nebular/theme';



@Component({
  selector: 'pop-player',
  template: `
    <nb-layout windowMode>

      <nb-layout-column left class="small player-left-column">
        <nb-user [picture]=player.avatar_url [name]=player.username [title]=player.pop_id size="xlarge"></nb-user>
        <nb-menu [items]="subMenu" class="player-menu-item"></nb-menu>
      </nb-layout-column>

      <nb-layout-column class="main-content player-main-content">
        <!--ng-content select="router-outlet"></ng-content-->
        <router-outlet></router-outlet>
      </nb-layout-column>

    </nb-layout>

  `,
})
export class PlayerComponent {
  subMenu: NbMenuItem[] = [
    {
      title: 'Perfil',
      icon: 'ion ion-android-radio-button-off',
      link: './profile',
    },
    /** {
      title: 'Datos',
      icon: 'ion ion-android-radio-button-off',
      link: './player-data-table',
    },
    {
      title: 'Economia',
      icon: 'ion ion-android-radio-button-off',
    }, */
    {
      title: 'Inventario',
      icon: 'ion ion-android-radio-button-off',
    },
    /*{
      title: 'Estadisticas',
      icon: 'ion ion-android-radio-button-off',
    },*/
    /*{
      title: 'Progreso',
      icon: 'ion ion-android-radio-button-off',
    },*/
    /*{
      title: 'Premios',
      icon: 'ion ion-android-radio-button-off',
    }, */
    {
      title: 'Sesiones',
      icon: 'ion ion-android-radio-button-off',
    },
    {
      title: 'Historial',
      icon: 'ion ion-android-radio-button-off',
    },
    {
      title: 'Jugadores',
      icon: 'ion ion-ios-people-outline',
      link: '/pages/players/players-table',
    },
  ];

  player = { pop_id: "g2c7c0042511c086" , username: 'noe139'     , avatar_url: 'assets/images/avatars/players/025' , session: '01:00', time: '09:12 pm', device_id: "afdsfadfa" , status: 'banned' , online: 'false', created: 'today'      , lastlogin: '8:30 am' };
  player_pop_id= "g2c7c0042511c086";
  player_avatar_url= 'assets/images/avatars/players/025';
}
