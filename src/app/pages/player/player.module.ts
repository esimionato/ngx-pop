import { NgModule } from '@angular/core';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { PlayerDataTableComponent } from './player-data-table/player-data-table.component';
import { PlayerEconomyTableComponent } from './player-economy-table/player-economy-table.component';
import { PlayerProfileComponent } from './player-profile/player-profile.component';
import { PlayerComponent } from './player.component';
import { PlayerRoutingModule, routedComponents } from './player-routing.module';

@NgModule({
  imports: [
    ThemeModule,
    PlayerRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
    PlayerDataTableComponent,
    PlayerEconomyTableComponent,
    PlayerComponent,
    PlayerProfileComponent,
  ],
})
export class PlayerModule { }
