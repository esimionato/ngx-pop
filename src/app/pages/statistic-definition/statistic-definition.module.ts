import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';

import { StatisticDefinitionComponent } from './statistic-definition.component';
import { StatisticDefinitionRoutingModule } from './statistic-definition-routing.module';
import { StatisticDefinitionTableComponent } from './statistic-definition-table/statistic-definition-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    ThemeModule,
    Ng2SmartTableModule,
    StatisticDefinitionRoutingModule
  ],
  declarations: [
    StatisticDefinitionComponent,
    StatisticDefinitionTableComponent
  ]
})
export class StatisticDefinitionModule { }
