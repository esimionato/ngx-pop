import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';
import { StatisticDefinitionSource } from './statistic-definition-source';

@Component({
  selector: 'app-statistic-definition-table',
  templateUrl: './statistic-definition-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class StatisticDefinitionTableComponent implements OnInit {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
    stCode: {
      title: 'Codigo',
      type: 'string',
      width: '10%'
    },
    stName: {
      title: 'Estadistica',
      type: 'string',
      width: '20%',
    },
    stInitialValue: {
      title: 'Inicial',
      type: 'number',
      width: '5%',
    },
    stMinValue: {
      title: 'Minimo',
      type: 'number',
      width: '5%',
    },
    stMaxValue: {
      title: 'Maximo',
      type: 'number',
      width: '5%',
    },
    aggregationMethod: {
      title: 'Agregacion',
      type: 'string',
      width: '8',
      editor: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'last', value: 'LAST'},
            {title: 'max', value: 'MAX'},
            {title: 'min', value: 'MIN'},
            {title: 'sum', value: 'SUM'},
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'last', value: 'LAST'},
            {title: 'max', value: 'MAX'},
            {title: 'min', value: 'MIN'},
            {title: 'sum', value: 'SUM'},
            {title: 'count', value: 'COUNT'},
          ]
        }
      }
    },
    resetFrequency: {
      title: 'Reinicio',
      type: 'string',
      width: '8',
      editor: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'manual', value: 'MANUALLY'},
            {title: 'hora', value: 'HOURLY'},
            {title: 'diario', value: 'DAILY'},
            {title: 'semanal', value: 'WEEKLY'},
            {title: 'mensual', value: 'MONTHLY'},
          ]
        }
      },
      filter: {
        type: 'list',
        config: {
          list: [
            {title: '', value: ''},
            {title: 'manual', value: 'MANUALLY'},
            {title: 'hora', value: 'HOURLY'},
            {title: 'diario', value: 'DAILY'},
            {title: 'semanal', value: 'WEEKLY'},
            {title: 'mensual', value: 'MONTHLY'},
          ]
        }
      }
    }

    }
  };

  source : StatisticDefinitionSource;

  constructor(protected http: HttpService) {
    this.source = new StatisticDefinitionSource(this.http);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.statisticDefinition+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.statisticDefinition, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.statisticDefinition, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

}
