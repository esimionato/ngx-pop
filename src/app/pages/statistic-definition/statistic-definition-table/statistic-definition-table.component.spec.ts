import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticDefinitionTableComponent } from './statistic-definition-table.component';

describe('StatisticDefinitionTableComponent', () => {
  let component: StatisticDefinitionTableComponent;
  let fixture: ComponentFixture<StatisticDefinitionTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticDefinitionTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticDefinitionTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
