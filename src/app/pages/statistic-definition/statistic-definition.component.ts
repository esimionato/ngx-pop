import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'statistic-definition',
  template: '<router-outlet></router-outlet>',
})

export class StatisticDefinitionComponent implements OnInit {

  constructor() { }
  ngOnInit() {
  }

}
