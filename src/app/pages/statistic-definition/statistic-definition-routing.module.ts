import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StatisticDefinitionComponent } from './statistic-definition.component'
import { StatisticDefinitionTableComponent } from './statistic-definition-table/statistic-definition-table.component'

const routes: Routes = [{
  path: '',
  component: StatisticDefinitionComponent,
  children: [{
    path: 'statistic-definition-table',
    component: StatisticDefinitionTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticDefinitionRoutingModule { }

export const routedComponents = [
  StatisticDefinitionTableComponent,
]
