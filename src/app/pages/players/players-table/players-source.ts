import { Injectable } from '@angular/core';

import { HttpService } from '../../../@core/utils/http.service';
import { LocalDataSource } from 'ng2-smart-table';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';



@Injectable()
export class PlayerSource extends LocalDataSource {

  lastRequestCount: number = 0;
  lastData: {};

  private players = {
    P001: { pop_id: "g2c7c0042511c086" , username: 'noe139'     , avatarUrl: 'assets/images/avatars/players/025' , session: '01:00', time: '09:12 pm', device_id: "afdsfadfa" , status: 'banned' , online: 'false', created: 'today'      , lastlogin: '8:30 am' } ,
    P002: { pop_id: "g2c7c0032511c073" , username: 'matilda7'   , avatarUrl: 'assets/images/avatars/players/008' , session: '00:30', time: '09:00 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '5 days ago' , lastlogin: '5:10 am'} ,
    P003: { pop_id: "g2c7c00428h1c0l6" , username: 'claudita11' , avatarUrl: 'assets/images/avatars/players/031' , session: '00:20', time: '09:40 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '9 days ago' , lastlogin: '4:00 am'} ,
    P004: { pop_id: "g2c7c0041511c08h" , username: 'juan22'     , avatarUrl: 'assets/images/avatars/players/028' , session: '00:22', time: '09:13 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '10 days ago', lastlogin: '0:30 am'} ,
    P005: { pop_id: "g2c7c00329000006" , username: 'kaky8'      , avatarUrl: 'assets/images/avatars/players/033' , session: '00:21', time: '10:00 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '11 days ago', lastlogin: 'Nov 16th 10:30 am'} ,
    P006: { pop_id: "g2c7c1042522ch26" , username: ''           , avatarUrl: 'assets/images/avatars/players/035' , session: '06:30', time: '10:30 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '11 days ago', lastlogin: 'Agus 10th 0:30 am'} ,
    P007: { pop_id: "g2c7c10190111116" , username: ''           , avatarUrl: 'assets/images/avatars/players/035' , session: '02:10', time: '10:30 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '11 days ago', lastlogin: '0:30 am'} ,
    P008: { pop_id: "g2c7c1009111l086" , username: ''           , avatarUrl: 'assets/images/avatars/players/034' , session: '00:05', time: '10:02 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '25 days ago', lastlogin: '0:30 am'} ,
    P009: { pop_id: "g2c7c10120111186" , username: ''           , avatarUrl: 'assets/images/avatars/players/003' , session: '00:02', time: '11:00 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '30 days ago', lastlogin: '0:30 am'} ,
    P010: { pop_id: "g2c7c1011533c086" , username: ''           , avatarUrl: 'assets/images/avatars/players/024' , session: '00:15', time: '11:10 pm', device_id: "afdsfadfa" , status: 'active' , online: 'true' , created: '30 days ago', lastlogin: '0:30 am'}
  };


  private playersArray: any[];

  constructor(protected http: HttpService) {
    super();
    this.playersArray = Object.values(this.players);
  }

  count(): number {
    return this.lastRequestCount;
  }

  getElements(): Promise<any> {
    //return this.http.doGet(this.http.paths.player).map(res => {
    //  return res.json();
    //}).toPromise();
    return Observable.of(this.playersArray).toPromise();
  }

  find(element: any): Promise<any> {
    return Promise.resolve(element);
  }

}
