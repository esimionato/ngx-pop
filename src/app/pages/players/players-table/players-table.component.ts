import { Component } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';

import { PlayerSource } from './players-source';

@Component({
  selector: 'players-table',
  templateUrl: './players-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    },
    .img-avatar {
      border-radius: 50%;
      max-width: 100%;
    }
  `],
})
export class PlayersTableComponent {


  settings = {
    actions: false,
    filter: false,
    pager: {
      display: true,
      perPage: '8',
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      pop_id: {
        title: '',
        type: 'html',/// type: 'string',
        editable: false,
        filter: true,
        width: '40%',
        valuePrepareFunction: (cell, row) => {
          return `<a href="/#/pages/player/profile/${row.popId}">`
            +" <nb-user size='large'> "
            +" <div class='user-container'> "
            +   " <div class='user-picture image' > "
            +     "  <img src=\""+ row.avatarUrl +"\" class='img-avatar'/> "
            +    "</div> "
            +   " <div class='info-container'> "
            +     "  <span class='user-title'>"+row.popId+"</span> | "
            +     "  <span class='user-name'>"+row.username+"</span>"
            +     "  <div class='user-subtitle'>+ device: "+row.deviceId+" </div> "
            +     "  <div class='user-subtitle'>+ email: "+row.email+" </div> "
            +   " </div> </div> </nb-user></a>";
        },
      },
      status: {
        title: 'Estado',
        type: 'html',
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'activo', value: 'active'},
              {title: 'suspendido', value: 'banned'},
            ]
          },
        },
        width: '3%',
        valuePrepareFunction: (cell, row) => {
          if (row.status=='banned') {
            return "<div class='player_status_banned'>suspendido</div>";
          } else {
            return "<div class='player_status_active'>activo</div>";
          }
        },
      },
      antiguedad: {
        title: 'Antiguedad',
        type: 'html',/// type: 'string',
        editable: false,
        filter: true,
        width: '40%',
        valuePrepareFunction: (cell, row) => {
          let now = new Date();
          let createdAt = new Date(row.createdAt);
          return (now.getDate()-createdAt.getDate()) + " dias";
        },
      },
      lastModifiedAt: {
        title: 'Ultimo Ingreso',
        type: 'html',/// type: 'string',
        editable: false,
        filter: true,
        width: '40%',
        valuePrepareFunction: (cell, row) => {
          let now = new Date();
          let createdAt = new Date(row.lastModifiedAt);
          return (now.getDate()-createdAt.getDate()) + " dias";
        },
      },
      online: {
        title: 'En Linea',
        type: 'html',
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'Online', value: 'true'},
            ]
          },
        },
        width: '3%',
        valuePrepareFunction: (cell, row) => {
          if (row.online==true) {
            return "<div class='on_line_on'>online</div>";
          }
          return "";
        },
      },
    },
  };


  source: PlayerSource;

  constructor(protected http: HttpService) {
    this.source = new PlayerSource(this.http);
  }

  ngOnInit() {
  }

  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.player+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.player, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.player, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }
}
