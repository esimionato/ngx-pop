import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { PlayersRoutingModule, routedComponents } from './players-routing.module';
import { PlayersService } from '../../@core/data/players.service';

@NgModule({
  imports: [
    ThemeModule,
    PlayersRoutingModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    PlayersService,
  ],
})
export class PlayersModule { }
