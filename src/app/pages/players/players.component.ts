import { Component } from '@angular/core';

@Component({
  selector: 'players',
  template: `<router-outlet></router-outlet>`,
})
export class PlayersComponent {
}
