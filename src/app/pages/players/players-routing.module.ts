import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlayersComponent } from './players.component';
import { PlayersTableComponent } from './players-table/players-table.component';

const routes: Routes = [{
  path: '',
  component: PlayersComponent,
  children: [{
    path: 'players-table',
    component: PlayersTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlayersRoutingModule { }

export const routedComponents = [
  PlayersComponent,
  PlayersTableComponent,
];
