import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommonModule } from '@angular/common';

import { HttpModule } from '@angular/http';

import { ThemeModule } from '../../@theme/theme.module';

import { CatalogComponent } from './catalog.component';
import { CatalogRoutingModule, routedComponents } from './catalog-routing.module';
import { CatalogItemTableComponent } from './catalog-item-table/catalog-item-table.component';

@NgModule({
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    HttpModule,
    ThemeModule,
    CatalogRoutingModule
  ],
  declarations: [
    CatalogComponent,
    CatalogItemTableComponent
  ]
})
export class CatalogModule {
}
