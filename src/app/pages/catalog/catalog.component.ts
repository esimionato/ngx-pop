import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'catalog',
  template: '<router-outlet></router-outlet>',
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
