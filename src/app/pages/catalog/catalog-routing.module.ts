import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatalogComponent } from './catalog.component';
import { CatalogItemTableComponent } from './catalog-item-table/catalog-item-table.component';

const routes: Routes = [{
  path: '',
  component: CatalogComponent,
  children: [{
    path: 'catalog-item-table',
    component: CatalogItemTableComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatalogRoutingModule { }

export const routedComponents = [
  CatalogComponent,
  CatalogItemTableComponent,
];
