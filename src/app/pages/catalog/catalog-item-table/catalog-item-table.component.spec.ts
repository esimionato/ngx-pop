import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogItemTableComponent } from './catalog-item-table.component';

describe('CatalogItemTableComponent', () => {
  let component: CatalogItemTableComponent;
  let fixture: ComponentFixture<CatalogItemTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogItemTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogItemTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
