import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../@core/utils/http.service';
import { CatalogItemSource } from './catalog-item-source';

@Component({
  selector: 'catalog-item-table',
  templateUrl: './catalog-item-table.component.html',
  styles: [`
    nb-card {
      transform: translate3d(0, 0, 0);
    }
  `],
})
export class CatalogItemTableComponent implements OnInit {

  settings = {
    pager: {
      display: true,
      perPage: 10,
    },
    add: {
      confirmCreate: true,
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      confirmSave: true,
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      confirmDelete: true,
      deleteButtonContent: '<i class="nb-trash"></i>',
    },
    columns: {
      itemImageUrl: {
        title: 'icono',
        type: 'html',
        width: '8%',
        valuePrepareFunction: (cell, row) => {
          return "<img src=\""+ row.itemImageUrl+"\" class='img-avatar'/>";
        }
      },
      itemCode: {
        title: 'Codigo',
        type: 'string',
        width: '10%',
      },
      displayName: {
        title: 'Nombre',
        type: 'string',
        width: '15%'
      },
      itemStatus: {
        title: 'Status',
        type: 'string',
        width: '8%',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'ON', value: 'ACTIVE'},
              {title: 'OFF', value: 'INACTIVE'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'ON', value: 'ACTIVE'},
              {title: 'OFF', value: 'INACTIVE'}
            ]
          }
        }
      },
      tags: {
        title: 'Tags',
        type: 'textarea',
        width: '20%'
      },
      consumable: {
        title: 'Consumible',
        type: 'string',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'permanente', value: 'DURABLE'},
              {title: 'uso', value: 'CONSUMABLE_BY_COUNT'},
              {title: 'tiempo', value: 'CONSUMABLE_BY_TIME'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'permanente', value: 'DURABLE'},
              {title: 'uso', value: 'CONSUMABLE_BY_COUNT'},
              {title: 'tiempo', value: 'CONSUMABLE_BY_TIME'}
            ]
          }
        },
        width: '2%',
      },
      stackable: {
        title: 'Stackeable',
        type: 'boolean',
        width: '2%',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        }
      },
      tradable: {
        title: 'Intercambable',
        type: 'boolean',
        width: '2%',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'no', value: 'false'},
              {title: 'si', value: 'true'}
            ]
          }
        }
      },
      limited: {
        title: 'EP',
        type: 'boolean',
        width: '2',
        editor: {
          type: 'list',
          config: {
            list: [
              {title: 'NO', value: 'false'},
              {title: 'SI', value: 'true'}
            ]
          }
        },
        filter: {
          type: 'list',
          config: {
            list: [
              {title: 'NO', value: 'false'},
              {title: 'SI', value: 'true'}
            ]
          }
        }
      },
      priceAmount: {
        title: 'Precio',
        type: 'number',
        width: '8%',
      },

    }
  };

  source: CatalogItemSource;

  constructor(protected http: HttpService) {
    this.source = new CatalogItemSource(this.http);
  }

  ngOnInit() {
  }


  onDeleteConfirm(event): void {
    console.log(".: delete_confirm :.");
    console.log(event.data.id);
    this.http.doDelete(this.http.paths.catalogItem+'/'+event.data.id).map(res => {
      return res.json();
    }).toPromise();
  }


  onSaveConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPut(this.http.paths.catalogItem, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }

  onCreateConfirm(event): Promise<any> {
    event.confirm.resolve();
    return this.http.doPost(this.http.paths.catalogItem, event.newData).map(res => {
      return res.json();
    }).toPromise();
  }


}
